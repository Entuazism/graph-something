﻿using System;
using System.Windows.Forms;

namespace GraphSomething {
	public partial class Form1 : Form {

		public DrawableListGraph graph;

		public Form1() {
			InitializeComponent();

			graph = new DrawableListGraph();
			graph.FromString(this.textBox1.Text);
			graph.DUpdate(this.radioButton2.Checked);
		}

		private void OnGraphChange(object sender, EventArgs e) {
			// refresh graph & image
			graph.FromString(this.textBox1.Text);

			string s1 = this.textBox3.Text + "  ";
			string s2 = this.textBox2.Text + "  ";
			graph.SetEnds(s1.Substring(0, 1), s1.Substring(1, 1));
			graph.SetIgnored(s2.Substring(0, 1), s2.Substring(1, 1));

			try {
				graph.DUpdate(this.radioButton2.Checked);
			} catch (Exception) {
				MessageBox.Show("Длительность вычислений слишком велика", "Вычисления прерваны");
			}
			graph.Draw(this.panel1.CreateGraphics());

			dataGridView1.Rows.Clear();
			int i = 0;
			foreach (var path in graph.paths) {
				dataGridView1.Rows.Add(++i, path.Join(" → "));
			}
		}

		private void button1_Click(object sender, EventArgs e) {
			graph.pathIndex++;
			graph.Draw(this.panel1.CreateGraphics());
			dataGridView1.Rows[graph.pathIndex].Selected = true;
		}

		private void panel1_Paint(object sender, PaintEventArgs e) {
			graph.Draw(this.panel1.CreateGraphics());

		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e) {
			if (this.dataGridView1.SelectedRows.Count == 0) return;
			graph.pathIndex = this.dataGridView1.SelectedRows[0].Index;
			graph.Draw(this.panel1.CreateGraphics());
		}

		private void textBox1_DragEnter(object sender, DragEventArgs e) {
			if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
		}

		private void textBox1_DragDrop(object sender, DragEventArgs e) {
			string[] files = (string[]) e.Data.GetData(DataFormats.FileDrop);
			this.textBox4.Text = files[0];
			this.textBox1.Text = System.IO.File.ReadAllText(this.textBox4.Text).Substring(0, 1000);
		}

		private void button2_Click(object sender, EventArgs e) {
			System.IO.File.WriteAllText(this.textBox4.Text, this.textBox1.Text);
		}
	}
}
