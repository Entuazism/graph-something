﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DimavasLibrary {

	/// <summary>
	/// Map (aka Dictionary) implementation from first lab. Should be used in second lexeme table generation.
	/// </summary>
	public class Map<TKey, TValue> : IEnumerable<Map<TKey, TValue>.KeyValuePair> where TKey : IComparable<TKey> {

		public class KeyValuePair {
			public TKey Key;
			public TValue Value;

			public KeyValuePair(TKey key, TValue value) {
				Key = key;
				Value = value;
			}
		}

		private KeyValuePair[] items;
		public int Size { get; private set; } = 0;
		private int arraySize = 0;

		public Map() {
			items = new KeyValuePair[10];
			arraySize = 10;
		}

		/// <summary>
		/// Using Binary Search to find nearest equal or lesser element
		/// </summary>
		private int FindNearest(TKey key) {
			int lt = 0, gt = Size - 1;
			while (lt < gt) {
				int mid = (lt + gt) / 2;
				int cmp = key.CompareTo(items[mid].Key);

				if (cmp > 0) // key is greater then item
					lt = mid + 1;
				else
					gt = mid;
			}

			if (lt >= Size || key.CompareTo(items[lt].Key) < 0)
				return lt - 1;
			else
				return lt;
		}

		private void Insert(int index, KeyValuePair pair) {
			if (Size == arraySize) {
				Array.Resize(ref items, arraySize * 2);
				arraySize *= 2;
			}
			for (int i = Size - 1; i >= index; i--) {
				items[i + 1] = items[i];
			}
			items[index] = pair;
			Size++;
		}

		public Map<TKey, TValue> Set(TKey key, TValue value) {
			if (Size == 0) {
				Insert(0, new KeyValuePair(key, value));
				return this;
			} else {
				int index = FindNearest(key);
				if (index < 0 || key.CompareTo(items[index].Key) > 0) {
					Insert(index + 1, new KeyValuePair(key, value));
				} else {
					items[index].Value = value;
				}
			}
			return this;
		}

		public bool Has(TKey key) {
			int index = FindNearest(key);
			return index >= 0 && key.CompareTo(items[index].Key) == 0;
		}

		public TValue Get(TKey key) {
			int index = FindNearest(key);
			if (index >= 0 && key.CompareTo(items[index].Key) == 0) {
				return items[index].Value;
			} else {
				return default;
			}
		}

		public bool Delete(TKey key) {
			int index = FindNearest(key);
			if (index < 0 || key.CompareTo(items[index].Key) != 0) {
				return false;
			}
			Size--;
			for (int i = index; i < Size; i++) {
				items[i] = items[i + 1];
			}
			items[Size] = default;
			return true;
		}

		public TValue this[TKey key] {
			get { return Get(key); }
			set { Set(key, value); }
		}

		public void DebugPrint() {
			Console.Write($"Map({Size}/{arraySize}) {{");
			if (Size > 0) {
				Console.Write($"{items[0].Key} => {items[0].Value}");
			}
			for (int i = 1; i < Size; i++) {
				Console.Write($", {items[i].Key} => {items[i].Value}");
			}
			Console.WriteLine($"}}");
		}

		/// <summary>
		/// Implementation of some of Enumerable interface
		/// Allows user to use `for..of` construction
		/// </summary>
		public class MapEnumerator : IEnumerator<KeyValuePair> {
			private Map<TKey, TValue> map;

			public MapEnumerator(Map<TKey, TValue> map) {
				this.map = map;
			}

			private int step = -1;
			public KeyValuePair Current { get { return map.items[step]; } }

			object IEnumerator.Current => (object) Current;

			public bool MoveNext() {
				if (++step >= map.Size) return false;
				return true;
			}

			public void Dispose() {
				this.map = null;
			}

			public void Reset() {
				throw new NotImplementedException();
			}
		}

		public MapEnumerator GetEnumerator() {
			return new MapEnumerator(this);
		}

		IEnumerator<KeyValuePair> IEnumerable<KeyValuePair>.GetEnumerator() {
			return this.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
	}

	/// <summary>
	/// A leftover from another one of first labs
	/// </summary>
	public class Deque<T> : IEnumerable<T> {

		private class Node {
			public T Value;
			public Node Prev;
			public Node Next;

			public Node(T value, Node prev, Node next) {
				Value = value;
				Prev = prev;
				Next = next;
			}
		}

		private Node first = null;
		private Node last = null;
		public int Length { get; private set; } = 0;

		private int Insert(T value, Node prev, Node next) {
			Node node = new Node(value, prev, next);

			if (prev == null)
				first = node;
			else
				prev.Next = node;

			if (next == null)
				last = node;
			else
				next.Prev = node;

			return ++Length;
		}

		private Node Cut(Node node) {
			if (node == first)
				first = first.Next;
			if (node == last)
				last = last.Prev;

			if (node.Prev != null) {
				node.Prev.Next = null;
				node.Prev = null;
			}
			if (node.Next != null) {
				node.Next.Prev = null;
				node.Next = null;
			}
			Length--;
			return node;
		}

		public int Push(T value) {
			return Insert(value, last, null);
		}

		public int Unshift(T value) {
			return Insert(value, null, first);
		}

		public T Pop() {
			if (Length == 0) return default;
			return Cut(last).Value;
		}

		public T Shift() {
			if (Length == 0) return default;
			return Cut(first).Value;
		}

		public T Peek() {
			return First;
		}

		public T First { get { return Length == 0 ? default : first.Value; } }
		public T Last { get { return Length == 0 ? default : last.Value; } }

		public T this[int i] {
			get { if (i >= Length) return default; Node n = first; while (i-- > 0) n = n.Next; return n.Value; }
			set { while (i >= Length) Push(default); Node n = first; while (i-- > 0) n = n.Next; n.Value = value; }
		}

		public void DebugPrint() {
			Console.Write($"Deque({Length}) {{");
			if (Length > 0) {
				Console.Write($"{first.Value}");
				for (Node node = first.Next; node != null; node = node.Next) {
					Console.Write($", {node.Value}");
				}
			}
			Console.WriteLine("}");
		}

		public override string ToString() {
			var b = new StringBuilder();
			b.Append($"Deque({Length}){{");
			bool first = true;
			foreach (var item in this) {
				if (!first) b.Append(",");
				b.Append(" ").Append(item);
				first = false;
			}
			b.Append(" }");
			return b.ToString();
		}

		/// <summary>
		/// Implementation of GetEnumerator
		/// Allows user to use `for..of` construction
		/// </summary>
		public IEnumerator<T> GetEnumerator() {
			var node = first;
			while (node != null) {
				yield return node.Value;
				node = node.Next;
			}
		}

		public Deque<T> Slice(int start = 0, int end = int.MaxValue) {
			Deque<T> copy = new Deque<T>();
			if (start < 0) start = Length + start;
			if (end < 0) end = Length + end;
			Node node = first;
			while (start != 0 && node != null) {
				start--;
				node = node.Next;
			}
			while (end > 0 && node != null) {
				end--;
				copy.Push(node.Value);
				node = node.Next;
			}
			return copy;
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator() {
			return this.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
	}

	/// <summary>
	/// List-based stack for graph
	/// </summary>
	public class Stack<T> : IEnumerable<T> {

		private class Node {
			public T value;
			public Node prev;

			public Node(T value, Node prev = null) {
				this.value = value;
				this.prev = prev;
			}
		}

		private Node end = null;
		public int Count { get; private set; } = 0;

		public T Peek() {
			return end is null ? default : end.value;
		}

		public T Pop() {
			if (end is null) return default;
			Count--;
			Node n = end;
			end = n.prev;
			return n.value;
		}

		public void Push(T value) {
			Count++;
			end = new Node(value, end);
		}

		public IEnumerator<T> GetEnumerator() {
			var node = end;
			while (node != null) {
				yield return node.value;
				node = node.prev;
			}
		}

		public override string ToString() {
			var b = new StringBuilder();
			b.Append($"Stack({Count}){{");
			bool first = true;
			foreach (var item in this) {
				if (!first) b.Append(",");
				b.Append(" ").Append(item);
				first = false;
			}
			b.Append(" }");
			return b.ToString();
		}

		public string Join(string delim) {
			var b = new StringBuilder();
			bool first = true;
			foreach (var item in this) {
				if (!first) b.Append(delim);
				b.Append(item);
				first = false;
			}
			return b.ToString();
		}

		public Stack<T> Reverse() {
			Stack<T> copy = new Stack<T>();
			Node node = end;
			while (node != null) {
				copy.Push(node.value);
				node = node.prev;
			}
			return copy;
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator() {
			return this.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.GetEnumerator();
		}
	}
}