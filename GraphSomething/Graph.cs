﻿using DimavasLibrary;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using ListNodeEnumerator = System.Collections.Generic.IEnumerator<GraphSomething.ListGraph.GraphNode>;
using Path = DimavasLibrary.Stack<GraphSomething.AbstractGraph.GraphNode>;

namespace GraphSomething {

	/// <summary>
	/// List-based graph impl
	/// </summary>
	public abstract class AbstractGraph {

		public abstract class GraphNode {
			public bool visited;
		}

		//- конструктор;
		//- метод для построения всех путей или циклов, удовлетворяющих некоторому
		//условию;
		//- абстрактный метод для поиска ещё не обработанной смежной вершины;
		//- абстрактный метод для проверки соответствия построенного пути или цикла
		//заданным условиям.

		public virtual System.Collections.Generic.IEnumerator<Path> DeepSearchEnumerator() {
			ResetSearch();
			Path path = new Path();

			while (true) {
				GraphNode next = FindConnectable(path);
				if (next is null) {
					if (path.Count == 0) yield break;
					path.Pop().visited = false;
					continue;
				}

				next.visited = true;
				path.Push(next);
				if (IsPathComplete(path)) {
					yield return path.Reverse();
				}
			}
		}

		public virtual Deque<Path> DeepSearch() {
			ResetSearch();
			Deque<Path> result = new Deque<Path>();
			var enumerator = DeepSearchEnumerator();
			while (enumerator.MoveNext()) result.Push(enumerator.Current);
			enumerator.Dispose();
			return result;
		}

		public virtual Deque<Path> WideSearch() {
			ResetSearch();
			Path path0 = new Path();

			Deque<Path> results = new Deque<Path>();

			Deque<Path> paths = new Deque<Path>();
			// fill first nodes:
			for (GraphNode next = FindConnectable(path0); next != null; next = FindConnectable(path0)) {
				var path = new Path();
				path.Push(next);
				paths.Push(path);
			}

			while (results.Length == 0 && paths.Length != 0) {
				Deque<Path> paths2 = new Deque<Path>();
				foreach (Path p in paths) {
					GraphNode next0 = FindConnectable(p);
					if (next0 is null) continue;
					paths2.Push(p);

					GraphNode next = FindConnectable(p);
					if (next != null) {
						Path r = p.Reverse();
						for (; next != null; next = FindConnectable(p)) {
							Path p2 = r.Reverse();
							p2.Push(next);
							paths2.Push(p2);
						}
					}
					p.Push(next0);
				}
				paths = paths2;
				foreach (Path p in paths) {
					p.Peek().visited = true;
					if (IsPathComplete(p)) {
						results.Push(p.Reverse());
					}
				}
			}

			return results;
		}

		public abstract void ResetSearch();

		public abstract GraphNode FindConnectable(Path path);

		public abstract bool IsPathComplete(Path path);
	}

	public class ListGraph : AbstractGraph {

		new public class GraphNode : AbstractGraph.GraphNode {
			public string name;
			public Deque<GraphNode> connections = new Deque<GraphNode>();
			public ListNodeEnumerator enumerator;

			public GraphNode(string name) {
				this.name = name;
			}

			public override string ToString() {
				return this.name;
			}
		}

		public Deque<GraphNode> nodes = new Deque<GraphNode>();
		public GraphNode firstOfPath;
		public GraphNode lastOfPath;
		public GraphNode ignoredEdge1;
		public GraphNode ignoredEdge2;

		public GraphNode startNode = new GraphNode("<start>");

		private Stopwatch stopwatch = new Stopwatch();

		public ListGraph AddNode(string name) {
			GraphNode added = new GraphNode(name);
			this.nodes.Push(added);
			this.startNode.connections.Push(added);
			return this;
		}

		public ListGraph SetIgnored(string name1, string name2) {
			ignoredEdge1 = this.nodes.FirstOrDefault(e => e.name == name1);
			ignoredEdge2 = this.nodes.FirstOrDefault(e => e.name == name2);
			return this;
		}

		public ListGraph SetEnds(string name1, string name2) {
			firstOfPath = this.nodes.FirstOrDefault(e => e.name == name1);
			lastOfPath = this.nodes.FirstOrDefault(e => e.name == name2);
			return this;
		}

		public ListGraph Connect(string name1, string name2) {
			GraphNode node1 = this.nodes.FirstOrDefault(e => e.name == name1);
			GraphNode node2 = this.nodes.FirstOrDefault(e => e.name == name2);
			if (node1 is null || node2 is null) return this;
			node1.connections.Push(node2);
			node2.connections.Push(node1);
			return this;
		}

		public override void ResetSearch() {
			foreach (GraphNode node in this.nodes) {
				node.visited = false;
			}
			startNode.visited = false;
			stopwatch.Restart();
		}

		private GraphNode currentNode;

		public override AbstractGraph.GraphNode FindConnectable(Path path) {
			if (path is null || path.Peek() is null) {
				currentNode = startNode;
			} else {
				currentNode = (GraphNode) path.Peek();
			}

			if (currentNode.enumerator is null) {
				currentNode.enumerator = currentNode.connections.GetEnumerator();
			}
			while (currentNode.enumerator.MoveNext()) {
				GraphNode next = currentNode.enumerator.Current;
				if (next.visited) continue;
				if (path.Count == 0 && next != firstOfPath) continue;
				if (next == ignoredEdge1 && path.Peek() == ignoredEdge2) continue;
				if (next == ignoredEdge2 && path.Peek() == ignoredEdge1) continue;
				return next;
			}
			currentNode.enumerator.Dispose();
			currentNode.enumerator = null;
			if (stopwatch.ElapsedMilliseconds > 100) {
				throw new Exception("Out of time");
			}
			return null;
		}

		public override bool IsPathComplete(Path path) {
			return path.Peek() == lastOfPath;
		}

		public override string ToString() {
			var b = new StringBuilder();
			b.Append(typeof(ListGraph)).AppendLine($"({nodes.Length}){{");
			foreach (GraphNode node in this.nodes) {
				b.Append($"  {node.name} => {{");

				bool first = true;
				foreach (GraphNode next in node.connections) {
					if (!first) b.Append(",");
					b.Append(" ").Append(next.name);
					first = false;
				}
				b.AppendLine(" }");
			}
			b.Append(" }");
			return b.ToString();
		}

		public string ToString2() {
			var b = new StringBuilder();

			b.AppendLine($"// nodes:");
			foreach (var n in this.nodes) {
				b.Append($"{n.name} ");
			}
			b.AppendLine();
			b.AppendLine($"// connections:");
			foreach (var n in this.nodes) {
				foreach (var c in n.connections) {
					b.Append($"{n.name}{c.name} ");
				}
				b.AppendLine();
			}
			return b.ToString();
		}

		public void FromString(string s) {
			this.Clear();
			char prev = ' ';
			foreach (char c in s) {
				if (prev == '/' && c != '\n') continue;
				if (c == '/') {
					prev = '/';
					continue;
				}

				if (!('a' <= c && c <= 'z' || 'A' <= c && c <= 'Z' || '0' <= c && c <= '9')) {
					prev = ' ';
					continue;
				}

				if (this.nodes.FirstOrDefault(e => e.name == $"{c}") is null)
					this.AddNode($"{c}");
				if (prev != ' ')
					if (this.nodes.First(e => e.name == $"{c}").connections.FirstOrDefault(e => e.name == $"{prev}") is null)
						this.Connect($"{c}", $"{prev}");

				prev = c;
			}
		}

		public void Clear() {
			this.nodes = new Deque<GraphNode>();
		}
	}

	public class DrawableListGraph : ListGraph {

		public class DrawableConnection {
			public DrawableNode start;
			public DrawableNode end;
			public int direction;
			public int selected;

			public DrawableConnection(DrawableNode start, DrawableNode end) {
				this.start = start;
				this.end = end;
			}
		}

		public class DrawableNode {
			public string name;
			public float x;
			public float y;
			public float radius;
			public int type;
			public int selected;

			private static Random random = new Random();

			public DrawableNode(string name, float x = 9999, float y = 9999, float radius = 10) {
				this.name = name;
				this.x = x == 9999 ? (float) random.NextDouble() * 200 - 100 : x;
				this.y = y == 9999 ? (float) random.NextDouble() * 200 - 100 : y;
				this.radius = radius;
			}
		}

		public Deque<DrawableNode> dNodes = new Deque<DrawableNode>();
		public Deque<DrawableConnection> dConnections = new Deque<DrawableConnection>();

		private DrawableConnection getConn(string start, string end, Deque<DrawableConnection> dConnections = null) {
			return (dConnections ?? this.dConnections).FirstOrDefault(e => e.start.name == start && e.end.name == end || e.start.name == end && e.end.name == start);
		}

		public Deque<Path> paths;
		public Path path;
		public int pathIndex;

		public void DUpdate(bool deep) {
			this.paths = deep ? DeepSearch() : WideSearch();
			if (this.paths.Length == 0) {
				this.pathIndex = 0;
				this.path = new Path();
				this.paths.Push(this.path);
			} else {
				this.pathIndex = pathIndex % this.paths.Length;
				this.path = this.paths[this.pathIndex];
			}

			Deque<DrawableNode> dn2 = new Deque<DrawableNode>();
			Deque<DrawableConnection> dc2 = new Deque<DrawableConnection>();

			// filter nodes
			foreach (var n in this.nodes) {
				DrawableNode dn = dNodes.FirstOrDefault(e => e.name == n.name) ?? new DrawableNode(n.name);
				dn2.Push(dn);
			}
			this.dNodes = dn2;

			// filter connections
			foreach (var n in this.nodes) {
				foreach (var c in n.connections) {
					DrawableConnection conn = getConn(n.name, c.name, dc2);
					if (conn != null) {
						if (conn.start.name == n.name)
							conn.direction |= 1;
						else
							conn.direction |= 2;
						continue;
					}
					conn = getConn(n.name, c.name);
					if (conn != null) {
						if (conn.start.name == n.name)
							conn.direction = 1;
						else
							conn.direction = 2;
						dc2.Push(conn);
						continue;
					}
					conn = new DrawableConnection(dNodes.First(e => e.name == n.name), dNodes.First(e => e.name == c.name));
					conn.direction = 1;
					dc2.Push(conn);
				}
			}
			this.dConnections = dc2;

			int i = 0;
			foreach (var n in this.dNodes) {
				n.x = (float) Math.Sin(Math.PI * 2 * i / this.dNodes.Length);
				n.y = (float) -Math.Cos(Math.PI * 2 * i / this.dNodes.Length);
				i++;
			}

			SetPath(this.path);
		}

		public void SetPath(Path p) {
			foreach (var n in this.dNodes) {
				n.selected &= ~0xF0;
				n.radius = 15;
			}
			foreach (var c in this.dConnections) {
				c.selected &= ~0xF0;
			}
			DrawableNode prev = null;
			foreach (GraphNode node in p) {
				DrawableNode next = dNodes.First(e => e.name == node.name);
				if (prev == null) {
					next.selected |= 0x10; // start
					next.radius = 18;
				}
				next.selected |= 0x20; // middle

				if (prev != null) {
					DrawableConnection conn = getConn(prev.name, next.name);
					if (conn.start.name == prev.name) {
						conn.selected |= 0x10; // forvard
					} else {
						conn.selected |= 0x20; // backvard
					}
				}

				prev = next;
			}
			if (getConn(ignoredEdge1?.name, ignoredEdge2?.name) != null)
				getConn(ignoredEdge1.name, ignoredEdge2.name).selected |= 0x40; // ignored
			if (prev != null) {
				prev.selected |= 0x40; // end
				prev.radius = 18;
			}
		}

		private Brush bWhite = new SolidBrush(Color.White);

		private Brush bNodeBg = new SolidBrush(Color.LightBlue);
		private Brush bNodeSelBg = new SolidBrush(Color.LightGreen);
		private Brush bNodeEndBg = new SolidBrush(Color.YellowGreen);
		private Brush bNodeText = new SolidBrush(Color.Black);

		private Pen pLine = new Pen(Color.Gray);
		private Pen pPathLine = new Pen(Color.Green, 2);
		private Pen pPathSkip = new Pen(Color.Red, 1.5f);

		private Font font = new Font("Times New Roman", 15);
		private StringFormat stringFormat = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };

		private SizeF localScale;
		private PointF localCenter;

		private PointF paneCenter;
		private SizeF paneScale;

		private const int borderWidth = 40;

		private void initCoords(Graphics g) {
			//g.VisibleClipBounds.
			paneCenter.X = (g.VisibleClipBounds.X + g.VisibleClipBounds.Width) / 2;
			paneCenter.Y = (g.VisibleClipBounds.Y + g.VisibleClipBounds.Height) / 2;
			paneScale.Width = g.VisibleClipBounds.Width;
			paneScale.Height = g.VisibleClipBounds.Height - borderWidth * 2;
			paneScale.Width = g.VisibleClipBounds.Width - borderWidth * 2;
			if (paneScale.Height > paneScale.Width) paneScale.Height = paneScale.Width; paneScale.Width = paneScale.Height;

			if (this.dNodes.Length == 0)
				this.dNodes.Push(new DrawableNode(""));

			float xmin = this.dNodes.First.x, ymin = this.dNodes.First.y, xmax = this.dNodes.First.x, ymax = this.dNodes.First.y;
			foreach (var node in this.dNodes) {
				if (xmin > node.x) xmin = node.x;
				if (xmax < node.x) xmax = node.x;
				if (ymin > node.y) ymin = node.y;
				if (ymax < node.y) ymax = node.y;
			}
			localCenter = new PointF((xmin + xmax) / 2, (ymin + ymax) / 2);
			localScale = new SizeF(xmax - xmin, ymax - ymin);
			if (localScale.Height < 0.1f) localScale.Height = 0.1f;
			if (localScale.Width < 0.1f) localScale.Width = 0.1f;
			if (localScale.Height > localScale.Width) localScale.Height = localScale.Width; localScale.Width = localScale.Height;
		}

		private float paneX(float x) {
			return (x - localCenter.X) / localScale.Width * paneScale.Width + paneCenter.X;
		}

		private float paneY(float y) {
			return (y - localCenter.Y) / localScale.Height * paneScale.Height + paneCenter.Y;
		}

		private RectangleF paneRect(float localX, float localY, float paneRadius) {
			float x = paneX(localX);
			float y = paneY(localY);
			return new RectangleF(x - paneRadius, y - paneRadius, paneRadius * 2, paneRadius * 2);
		}

		private Rectangle paneRect(float x1, float y1, float x2, float y2) {
			x1 = paneX(x1);
			x2 = paneX(x2);
			y1 = paneY(y1);
			y2 = paneY(y2);
			return new Rectangle((int) x1, (int) y1, (int) (x2 - x1), (int) (y2 - y1));
		}

		private PointF panePoint(float localX, float localY) {
			return new PointF(paneX(localX), paneY(localY));
		}

		public void Draw(Graphics g) {
			this.pathIndex = Math.Abs(this.pathIndex) % this.paths.Length;
			this.path = this.paths[this.pathIndex];
			SetPath(this.path);

			initCoords(g);

			g.FillRectangle(bWhite, g.VisibleClipBounds);
			//g.DrawRectangle(pBlack, paneRect(-50, -50, 50, 50));
			//g.DrawRectangle(pBlack, new Rectangle(0, 0, (int) paneScale.Width + 2 * borderWidth, (int) paneScale.Height + 2 * borderWidth));

			SetPath(this.path);

			foreach (DrawableConnection con in this.dConnections) {
				DrawConnection(g, con);
			}

			foreach (DrawableNode node in this.dNodes) {
				DrawNode(g, node);
			}
		}

		private void DrawNode(Graphics g, DrawableNode node) {
			Brush b = ((node.selected & 0x50) != 0) ? bNodeEndBg : ((node.selected & 0x20) != 0) ? bNodeSelBg : bNodeBg;
			g.FillEllipse(b, paneRect(node.x, node.y, node.radius));
			g.DrawString(node.name, font, bNodeText, panePoint(node.x, node.y), stringFormat);
		}

		private void DrawConnection(Graphics g, DrawableConnection con) {
			Pen pen = ((con.selected & 0x40) != 0) ? pPathSkip : (con.selected & 0x30) != 0 ? pPathLine : pLine;
			g.DrawLine(pen, panePoint(con.start.x, con.start.y), panePoint(con.end.x, con.end.y));
		}
	}
}