﻿using DimavasLibrary;
using GraphSomething;
using System;

//using System.Collections.Generic;
using System.Linq;

namespace GraphConsole {

	internal class Program {

		private static void Main(string[] args) {
			// Deque test
			Deque<int> s = new Deque<int>();
			foreach (var item in s) {
				Console.WriteLine(item);
			}

			Console.WriteLine(s.ToString());

			s.Push(1);
			s.Push(2);
			s.Push(3);

			foreach (var item in s) {
				Console.WriteLine(item);
			}
			Console.WriteLine(s.First(e => e == 2)); // throws on not found

			Console.WriteLine(s.ToString());

			Console.WriteLine(s.FirstOrDefault(e => e == 2));

			Map<int, int> map = new Map<int, int>();
			map.Set(088, 456);
			map.Set(123, 889);
			foreach (var item in map) {
				Console.WriteLine(item.Key);
			}

			ListGraph g = new ListGraph();
			g.AddNode("A").AddNode("B").AddNode("C").AddNode("D").AddNode("E");
			Console.WriteLine(g);

			g.Connect("A", "B").Connect("A", "C").Connect("A", "D").Connect("B", "E").Connect("C", "E").Connect("D", "E")
				.Connect("B", "C").Connect("C", "D");
			Console.WriteLine(g);

			g.SetEnds("A", "E");
			g.SetIgnored("B", "E");
			var r = g.WideSearch();
			Console.WriteLine(r);
			r = g.DeepSearch();
			Console.WriteLine(r);

			Console.WriteLine(g.ToString2());
			g.FromString(
			"// nodes:\n" +
			"A B C D E\n" +
			"// connections:\n" +
			"AB AC AD\n" +
			"BA BE BC\n" +
			"CA CE CB CD\n" +
			"DA DE DC\n" +
			"EB EC ED\n");

			Console.WriteLine(g.ToString2());
		}
	}
}